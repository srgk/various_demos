# Include standard modules
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from base_entity import BaseEntity
from product import Product


class LicenseType(BaseEntity):
    """
    LicenseType is a type of the license on some product
    like Professional, Student and Open Source for PyCharm.
    """
    def __init__(self, domain_id: UUID, title: str, product: Product):
        """
        A constructor of a license type

        :param domain_id: a unique identifier of this Entity
        :param title: some title of this license type (like "Professional")
        :param product: a product which this license is related to
        """
        super().__init__(domain_id)

        self._title = title
        self._product = product
        self._description = None

    @property
    def title(self) -> str:
        """
        Returns some title of this license type (like "Professional")

        :return: string, a title of the license type
        """
        return self._title

    @property
    def product(self) -> Product:
        """
        Returns an instance of Product which this license is related to

        :return: an instance of Product
        """
        return self._product

    @property
    def description(self) -> str or None:
        """
        Returns some description of this LicenseType (like its features
        or just a link to the info page)

        This field can be unfilled (i.e. equal to None / null)

        :return: string, a short description of this license type
        """
        return self._description

    @description.setter
    def description(self, new_desc: str or None) -> None:
        """
        Sets some description of this LicenseType

        This field can be unfilled or cleaned by specifying of None
        parameter value

        :param new_desc: a description, can be null
        :return: None
        """
        self._description = new_desc
