# Include standard modules

# Include 3rd-party modules
from uuid import UUID

# Include Upport modules


class BaseEntity(object):
    """
    Base entity is a utility base class for all entity classes
    in Upport system
    """
    def __init__(self, domain_id: UUID):
        """
        Constructor. Accepts only one parameter: an unique identifier
        of this entity instance.

        :param domain_id: an unique identifier of this entity
        """
        assert isinstance(domain_id, UUID)  # TODO: Raise an exception

        self._domain_id = domain_id
        self._database_id = None  # type: int or None

    @property
    def domain_id(self) -> str:
        """
        Returns a unique identifier of this Entity object

        :return: UUID
        """
        return str(self._domain_id)

    @property
    def database_id(self) -> int:
        """
        Returns an identifier of this Entity in database

        :return: int, a database object identifier
        """
        return self._database_id
