# Include standard modules
from uuid import UUID

# Include 3rd-party modules

# Include Upport modules
from base_entity import BaseEntity


# FIXME: Implement product subsystems and releases collections

class UnrelatedSubsystemError(ValueError):
    """
    This error indicates that the subsystem passed is unrelated
    to this Product and thus can't be saved
    """
    pass


class Product(BaseEntity):
    """
    Product is an abstraction of software product that is purchased
    or fetched by Customers and need te be supported
    """
    def __init__(self, domain_id: UUID, product_name: str, product_page: str = None):
        """
        Constructs a new product with a product name specified and
        an optional product page.

        :param domain_id: a unique identifier of this Entity
        :param product_name: the name of this product
        :param product_page: a link to product-related page
        """
        super().__init__(domain_id)

        self._product_name = product_name  # type: str
        self._product_page = product_page  # type: str or None

    @property
    def product_name(self) -> str:
        """
        Returns a name of this product. Like PyCharm or Intellij IDEA

        :return: string, a name of this product
        """
        return self._product_name

    @property
    def product_page(self) -> str or None:
        """
        Returns a URL to the product page (website).

        This field can be empty (is null, equal to None).

        :return: string, a url to the product page
        """
        return self._product_page

    @product_page.setter
    def product_page(self, new_page: str or None) -> None:
        """
        A setter for the product page URL. None value is acceptable
        (and allows to clear the field value)

        :return: None
        """
        self._product_page = new_page
