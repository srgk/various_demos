from sqlalchemy import Table, MetaData, Column, Integer, String, ForeignKey, Sequence, PickleType, create_engine
from sqlalchemy.orm import mapper, create_session, sessionmaker, relationship
from sqlalchemy.engine import Engine

from sqlalchemy_utils.types.uuid import UUIDType

from product import Product
from license_type import LicenseType


class DbSession(object):
    Session = sessionmaker()

    def __init__(self):
        self.metadata = MetaData()

        # WARNING:
        # Autoincrement works only for PKs. For other columns
        # use sqlalchemy.Sequence object

        # WARNING 2:
        # There is NO sequences in MySQL. So Sequence construction
        # is ignored in this case ¯\_(ツ)_/¯

        self.product_table = Table(
            'product', self.metadata,
            Column('_database_id', Integer, primary_key=True),
            Column('_domain_id', UUIDType, unique=True),
            Column('_product_name', String(50)),
            Column('_product_page', String(50))
            )

        self.license_type_table = Table(
            'license_type', self.metadata,
            Column('_database_id', Integer, primary_key=True),
            Column('_domain_id', UUIDType, unique=True),
            Column('_product_id', Integer, ForeignKey('product._database_id')),
            Column('_title', String(50)),
            Column('_description', String(200))
        )

        self.engine = None

    def map(self):
        mapper(Product, self.product_table)
        mapper(LicenseType, self.license_type_table, properties={
            '_product': relationship(Product)
        })

        self.metadata.create_all()

    def bind_engine(self, engine: Engine) -> None:
        self.metadata.bind = engine

        self.engine = engine

    def start_session(self) -> Session:
        return self.Session(bind=self.engine)
