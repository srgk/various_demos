from sqlalchemy import create_engine
from db_session import DbSession
from product import Product
from license_type import LicenseType
import uuid

from typing import Sequence

if __name__ == '__main__':
    dbs = DbSession()

    # engine = create_engine('sqlite:///:memory:', echo=True)
    engine = create_engine(
        "mysql+pymysql://pycharm_tester:pycharm_tester@localhost/pycharm_tester_alchemy?host=localhost?port=3306",
        echo=True
    )

    dbs.bind_engine(engine)
    dbs.map()

    p = Product(
        domain_id=uuid.uuid4(),
        product_name="Test product"
    )

    lt = LicenseType(
        uuid.uuid4(),
        title="Professional",
        product=p
    )

    lt2 = LicenseType(
        uuid.uuid4(),
        title="Ultimate",
        product=p
    )

    print(p.domain_id)

    s = dbs.start_session()

    s.add(p)
    s.add(lt)
    s.add(lt2)
    s.commit()

    lt2.description = "new desc"

    s.commit()

    lt2.description = "new desc2"

    fetched = s.query(Product)

    print(fetched.all())
    print(p)

    print(p.domain_id)

    del p
    del lt

    lt_fetched = s.query(LicenseType)

    print(lt_fetched.count())

    print("\n", isinstance(lt_fetched.all(), Sequence), "\n")

    print(lt_fetched.all()[0])
    print(lt_fetched.all()[0].domain_id)

    print(fetched.all()[0])
    print(fetched.all()[0].domain_id)
